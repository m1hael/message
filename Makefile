#
# Build script for Message service program
#

#-----------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library for the service program.
# the rpg modules and the binder source file are also created in BIN_LIB.
# binder source file and rpg module can be remove with the clean step (make clean)
BIN_LIB=QGPL

# to this library the prototype source file (copy book) is copied in the install step
INCLUDE=/usr/local/include

DEF=THREAD_SAFE

TGTRLS=*CURRENT

# RCFLAGS = RPG compile parameter
RCFLAGS=OPTION(*SRCSTMT) DBGVIEW(*LIST) DEFINE($(DEF)) OPTIMIZE(*BASIC) STGMDL(*INHERIT) TGTRLS($(TGTRLS))

# LFLAGS = binding parameter
LFLAGS=STGMDL(*INHERIT) TGTRLS($(TGTRLS))

#
# User-defined part end
#-----------------------------------------------------------


SOURCE_CCSID=819
OBJECTS = message
 
 
.SUFFIXES: .rpgle
 
# suffix rules
.rpgle:
	system -i "CHGATR OBJ('$<') ATR(*CCSID) VALUE($(SOURCE_CCSID))"
	system -i "CRTRPGMOD $(BIN_LIB)/$@ SRCSTMF('$<') $(RCFLAGS)"
        
all: clean compile bind clean
 
message:

compile: $(OBJECTS)

bind:
	-system -kpieb "DLTOBJ OBJ($(BIN_LIB)/MESSAGESRV) OBJTYPE(*FILE)"
	system -kpieb "CRTSRCPF FILE($(BIN_LIB)/MESSAGESRV) RCDLEN(200)"
	system -kpieb "CPYFRMSTMF FROMSTMF('message.bnd') TOMBR('/QSYS.LIB/$(BIN_LIB).LIB/MESSAGESRV.FILE/MESSAGE.MBR') MBROPT(*ADD)"
	system "CRTSRVPGM $(BIN_LIB)/MESSAGE MODULE($(BIN_LIB)/MESSAGE) $(LFLAGS) EXPORT(*SRCFILE) SRCFILE($(BIN_LIB)/MESSAGESRV) TEXT('Message')" 
	-system -kpieb "DLTOBJ OBJ($(BIN_LIB)/MESSAGESRV) OBJTYPE(*FILE)"
	
install: message.bnd
	-mkdir $(INCLUDE)/message
	cp message_h.rpgle $(INCLUDE)

clean:
	-system "DLTMOD $(BIN_LIB)/MESSAGE"

purge: clean
	-system "DLTSRVPGM $(BIN_LIB)/MESSAGE"
	
.PHONY:
